import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AppDataProvider } from '../../providers/app-data/app-data';
import { ProfilePage } from '../profile/profile';
import { SearchPage } from '../search/search';
//import { ArtistPage } from '../artist/artist';
//import { PlaylistPage } from '../playlist/playlist';
import { DetailPage } from '../detail/detail';
import { DetailPlaylistPage } from '../detail-playlist/detail-playlist';
import { LikePage } from '../like/like';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  urlPrueba;
  accessSpotify:any;
  countryHome:any;
  newRelease:any;
  categoriesHome:any;
  categoriesComplete:any;

  constructor(public navCtrl: NavController, public appData:AppDataProvider) {
  	this.urlPrueba = this.appData.getHomeUrl();
    this.accessSpotify = this.appData.spotifyApi;  
    this.countryHome = this.appData.country.code;
    this.categoriesHome = this.appData.returnCategories();
    this.categoriesComplete = [];
    console.log('CATEGORIES');
    console.log(this.categoriesHome);
    this.appData.getUserId();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
    /* NUEVOS LANZAMIENTOS */
    this.accessSpotify.getNewReleases({ limit : 5, offset: 0, country: this.countryHome })
     .then(data => {
          const response: any = data.body;
          //console.log(response);
          this.newRelease = response.albums.items;
        }, function(err) {
          console.log('Something went wrong!', err);
    });
     /*  */
    for (let i in this.categoriesHome) {
         // 1, "string", false
        console.log('GoFOR');
        console.log(this.categoriesHome[i].id);
        this.accessSpotify.getPlaylistsForCategory(this.categoriesHome[i].id, {
          country: this.countryHome,
          limit : 2,
          offset : 0
        })
        .then(data => {
          const response: any = data.body;
          //console.log(response);
          //this.categoriesComplete = response;          
            for (let x in response.playlists.items) {
              console.log(x);
              this.categoriesComplete.push(response.playlists.items[x]);
              console.log(this.categoriesComplete);
            }   
            
          }, function(err) {
          console.log('Something went wrong!', err);
        });
    }
    
  }
  goRelease(id){
    this.navCtrl.push(DetailPage,{idAlbum:id});
  }
  playlistDetail(id, owner){
    this.navCtrl.push(DetailPlaylistPage,{playlistId:id, ownerId:owner});
  }
  /**/
  getMe(){
  	this.navCtrl.setRoot(ProfilePage);
  };

  getHomeSearch(){
     this.navCtrl.push(SearchPage);
  }

  getLikesPage(){
    this.navCtrl.setRoot(LikePage);
  }
 
}
