webpackJsonp([10],{

/***/ 109:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RelatedartistPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_app_data_app_data__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__artist_artist__ = __webpack_require__(42);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the RelatedartistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RelatedartistPage = (function () {
    function RelatedartistPage(navCtrl, navParams, appData) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.appData = appData;
        this.relatedUp = false;
        this.accessSpotify = this.appData.spotifyApi;
        this.idRelatedArtist = this.navParams.get('idArtist');
    }
    RelatedartistPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad RelatedartistPage');
        //GetRelated
        this.accessSpotify.getArtistRelatedArtists(this.idRelatedArtist)
            .then(function (data) {
            console.log('Artist Data');
            var response = data.body;
            _this.relatedArtist = response.artists;
            _this.relatedUp = true;
            console.log(_this.relatedArtist);
        }, function (err) {
            console.log('Something went wrong!', err);
        });
    };
    RelatedartistPage.prototype.getArtist = function (id) {
        console.log("Artist from Related Artist");
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__artist_artist__["a" /* ArtistPage */], { paramId: id });
    };
    return RelatedartistPage;
}());
RelatedartistPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-relatedartist',template:/*ion-inline-start:"D:\htdocs\Ionic\sincronizapc\src\pages\relatedartist\relatedartist.html"*/'<!--\n  Generated template for the RelatedartistPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-icon id="app-icon"><img src="./assets/imgs/logo.png" alt=""></ion-icon>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n	<h1>Artistas Relacionados</h1>\n	<ion-grid *ngIf="relatedUp">\n	    <ion-row>\n	      <ion-col col-6 *ngFor="let related of relatedArtist">\n	        <div class="cont-artist" (click)="getArtist(related.id)">\n	          <div class="img-thumb" *ngIf="related.images">\n	          	<img src="{{related.images[1]?.url}}" alt="">\n	          </div>\n	          <h2>{{related.name}}</h2>\n	          <span>descubrir</span>\n	        </div>\n	      </ion-col>	           \n	    </ion-row>\n  	</ion-grid>\n</ion-content>\n'/*ion-inline-end:"D:\htdocs\Ionic\sincronizapc\src\pages\relatedartist\relatedartist.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_app_data_app_data__["a" /* AppDataProvider */]])
], RelatedartistPage);

//# sourceMappingURL=relatedartist.js.map

/***/ }),

/***/ 110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LikePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__search_search__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_app_data_app_data__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__artist_artist__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__detail_playlist_detail_playlist__ = __webpack_require__(43);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the LikePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LikePage = (function () {
    function LikePage(navCtrl, navParams, appData) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.appData = appData;
        this.existFavorites = false;
        this.favorites = this.appData.returnFavorites();
        this.checkFavorite();
    }
    LikePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LikePage');
    };
    LikePage.prototype.checkFavorite = function () {
        if (this.favorites.length > 0) {
            this.existFavorites = true;
            console.log("lista de favoritos");
            console.log(this.favorites);
        }
        else {
            this.existFavorites = false;
        }
    };
    // Detail
    LikePage.prototype.detailFavorite = function (id, owner, type) {
        if (type == 'artist') {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__artist_artist__["a" /* ArtistPage */], { paramId: id });
        }
        else if (type == 'playlist') {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__detail_playlist_detail_playlist__["a" /* DetailPlaylistPage */], { playlistId: id, ownerId: owner });
        }
    };
    // Delete
    LikePage.prototype.deleteFavorite = function (id) {
        this.appData.deleteFavorite(id);
        this.checkFavorite();
    };
    /**/
    LikePage.prototype.getHomeSearch = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__search_search__["a" /* SearchPage */]);
    };
    return LikePage;
}());
LikePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-like',template:/*ion-inline-start:"D:\htdocs\Ionic\sincronizapc\src\pages\like\like.html"*/'<!--\n  Generated template for the LikePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-icon id="app-icon"><img src="./assets/imgs/logo.png" alt=""></ion-icon>\n    <button class="icon-home" (click)="getHomeSearch()"><img src="./assets/icon/icon_search.png" alt=""></button>\n  </ion-navbar>\n</ion-header>\n\n\n\n<ion-content>\n  <div *ngIf="!existFavorites" class="noFav">No tienes favoritos</div>\n  <div col-12 *ngIf="existFavorites">\n    <div class="cont-like" *ngFor="let favorite of favorites" >\n      <div class="cont-img"><img src="{{favorite.url}}"></div>   \n      <div class="info">\n        <h1>{{favorite.name}}</h1>\n        <span>{{favorite.type}}</span>\n      </div>\n      <button class="detail" (click)="detailFavorite(favorite.id,favorite.owner,favorite.type)">Ver detalles</button>\n      <button class="delete" (click)="deleteFavorite(favorite.id)">eliminar</button>\n    </div>\n  </div>\n  \n\n</ion-content>\n'/*ion-inline-end:"D:\htdocs\Ionic\sincronizapc\src\pages\like\like.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_app_data_app_data__["a" /* AppDataProvider */]])
], LikePage);

//# sourceMappingURL=like.js.map

/***/ }),

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_app_data_app_data__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_in_app_browser__ = __webpack_require__(167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__intro_intro__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__home_home__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginPage = (function () {
    //**/
    function LoginPage(navCtrl, navParams, appData, inBrowser) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.appData = appData;
        this.inBrowser = inBrowser;
        this.isAuth = false;
        this.id_clientL = this.appData.id_client;
        this.client_secretL = this.appData.client_secret;
        this.url_redirectL = this.appData.url_redirect;
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
        this.urlLogin = 'https://accounts.spotify.com/authorize?client_id=' + this.id_clientL + '&response_type=token&redirect_uri=' + this.url_redirectL + '&scope=user-read-private%20user-read-email&state=34fFs29kd09';
    };
    LoginPage.prototype.loginAuth = function () {
        var _this = this;
        var browser = this.inBrowser.create(this.urlLogin, "_blank", "location=yes");
        browser.on('loadstart').subscribe(function (ev) {
            if (0 === ev.url.indexOf('http://localhost/callback')) {
                console.log(ev.url);
                _this.isAuth = true;
                _this.passAccessToken(ev.url);
                browser.close();
            }
        });
        /*local
        const urlLocal = "http://localhost/callback#access_token=BQBleK8q85_rhFVkEUmLPQSFNmexdrLvkugUum0LioL3MKaf53gqambWSihbBIT4H2GYxaWM-ahkoa58wQINKuRcDKM8DsyjKYy5wOugzA2qj4R9u4anS2RF-iyCBjbt8WmUfdRVvzsI6_SBmaimATm6rfN0&token_type=Bearer&expires_in=3600&state=34fFs29kd09";
        this.passAccessToken(urlLocal);*/
    };
    /*goSelectP(){
      this.navCtrl.setRoot(IntroPage);
    }*/
    //
    LoginPage.prototype.passAccessToken = function (url) {
        if (url == "") {
            console.log("no inicio sesion en spotify");
        }
        else {
            this.appData.getLogin(url);
            console.log(this.appData.savedCategory);
            if (typeof this.appData.savedCategory !== 'undefined' && this.appData.savedCategory.length > 0) {
                console.log("Debe ir a home");
                this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__home_home__["a" /* HomePage */]);
            }
            else {
                console.log("Debe ir a intro");
                this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__intro_intro__["a" /* IntroPage */]);
            }
            //this.navCtrl.setRoot(HomePage);
            //this.navCtrl.setRoot(IntroPage);
        }
    };
    return LoginPage;
}());
LoginPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-login',template:/*ion-inline-start:"D:\htdocs\Ionic\sincronizapc\src\pages\login\login.html"*/'<!--\n  Generated template for the LoginPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n<ion-content padding class="back-login">\n	<img src="./assets/imgs/logo.png" alt="" id="logo-c">\n	<h1>Sincroniza</h1>\n	<p>\n		Descubre nueva música a traves de tu música\n	</p>\n	<button ion-button (click)="loginAuth();">Iniciar Aplicación</button>\n</ion-content>\n'/*ion-inline-end:"D:\htdocs\Ionic\sincronizapc\src\pages\login\login.html"*/,
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__providers_app_data_app_data__["a" /* AppDataProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__providers_app_data_app_data__["a" /* AppDataProvider */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__ionic_native_in_app_browser__["a" /* InAppBrowser */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__ionic_native_in_app_browser__["a" /* InAppBrowser */]) === "function" && _d || Object])
], LoginPage);

var _a, _b, _c, _d;
//# sourceMappingURL=login.js.map

/***/ }),

/***/ 112:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlaylistPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_app_data_app_data__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Subject__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Subject__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the PlaylistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PlaylistPage = (function () {
    function PlaylistPage(navCtrl, navParams, appData) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.appData = appData;
        this.playlistSelected$ = new __WEBPACK_IMPORTED_MODULE_3_rxjs_Subject__["Subject"]();
        this.categoriesPlaylist = this.appData.returnCategories();
        this.countryPlaylist = this.appData.country.code;
        this.accessSpotify = this.appData.spotifyApi;
        this.playlistSelected$.subscribe(function (word) { return _this.changePlaylist(word); });
    }
    PlaylistPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PlaylistPage');
        this.accessSpotify.getPlaylistsForCategory('party', {
            country: 'BR',
            limit: 2,
            offset: 0
        })
            .then(function (data) {
            console.log(data.body);
        }, function (err) {
            console.log("Something went wrong!", err);
        });
    };
    PlaylistPage.prototype.changePlaylist = function (word) {
        console.log(word);
        this.accessSpotify.getPlaylistsForCategory(word, {
            country: 'EC',
            limit: 10,
            offset: 0
        })
            .then(function (data) {
            console.log(data.body);
        }, function (err) {
            console.log("Something went wrong!", err);
        });
    };
    return PlaylistPage;
}());
PlaylistPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-playlist',template:/*ion-inline-start:"D:\htdocs\Ionic\sincronizapc\src\pages\playlist\playlist.html"*/'<!--\n  Generated template for the PlaylistPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-icon id="app-icon"><img src="./assets/imgs/logo.png" alt=""></ion-icon>\n    <button class="icon-home"><img src="./assets/icon/icon_notification.png" alt=""></button>\n    <button class="icon-home" (click)="getHomeSearch()"><img src="./assets/icon/icon_search.png" alt=""></button>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n<section id="playlist">\n    <div class="title-select">\n      <div class="title"><span>Nueva Música</span><br/>para ti</div>\n      <div class="select-c">\n        <ion-list>\n          <ion-label class="label-select">tu país</ion-label>   \n          <ion-item>      \n            <ion-select class="select-country" [(ngModel)]="local" (ionChange)="playlistSelected$.next(local)">\n              <ion-option *ngFor="let category of categoriesPlaylist" [value]="category.id">\n                {{category.name}}\n              </ion-option>\n            </ion-select>\n          </ion-item>\n        </ion-list>\n      </div>\n    </div>\n  </section>\n  <ion-grid>\n    <ion-row>\n      <ion-col col-6 class="cont-playlist">\n        <img src="./assets/imgs/thumb.png" alt="">\n        <div class="over-playlist">\n          <h2>Nombre del playlist</h2>\n          <span>ver playlist</span>\n        </div>  \n      </ion-col>                \n    </ion-row>\n  </ion-grid>\n</ion-content>\n'/*ion-inline-end:"D:\htdocs\Ionic\sincronizapc\src\pages\playlist\playlist.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_app_data_app_data__["a" /* AppDataProvider */]])
], PlaylistPage);

//# sourceMappingURL=playlist.js.map

/***/ }),

/***/ 120:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 120;

/***/ }),

/***/ 13:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppDataProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(163);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(252);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_spotify_web_api_node__ = __webpack_require__(253);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_spotify_web_api_node___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_spotify_web_api_node__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(166);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/*
  Generated class for the AppDataProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var AppDataProvider = (function () {
    /***/
    function AppDataProvider(http, localStorage) {
        this.http = http;
        this.localStorage = localStorage;
        this.id_client = '1ae71dc7c28b44acb5a5badbf530c25d';
        this.client_secret = '453debdf796e4339823730208cd6316a';
        this.url_redirect = 'http://localhost/callback';
        this.scopes = ['user-read-private', 'user-read-email'];
        this.states = 'some-state-of-my-choice';
        this.getAuth = false;
        this.spotifyApi = new __WEBPACK_IMPORTED_MODULE_3_spotify_web_api_node___default.a({
            clientId: this.id_client,
            clientSecret: this.client_secret,
            redirectUri: this.url_redirect
        });
        /*Country Options*/
        this.COUNTRY_SELECTED_KEY = 'COUNTRY';
        //public countryCode:any;
        this.countries = [
            {
                id: 1,
                name: 'Ecuador',
                flag: 'ecuador.jpg',
                code: 'EC'
            },
            {
                id: 2,
                name: 'España',
                flag: 'espana.jpg',
                code: 'ES'
            },
            {
                id: 1,
                name: 'Argentina',
                flag: 'argentina.jpg',
                code: 'AR'
            },
            {
                id: 1,
                name: 'México',
                flag: 'mexico.jpg',
                code: 'MX'
            },
            {
                id: 1,
                name: 'EEUU',
                flag: 'usa.jpg',
                code: 'US'
            },
            {
                id: 1,
                name: 'Gran Bretaña',
                flag: 'uk.jpg',
                code: 'GB'
            }
        ];
        /* Categories */
        this.CATEGORIES_SELECTED_KEY = 'Categories';
        /**/
        /* SavedFavorites*/
        this.ARTIST_PLAYLIST_FAVORITE = 'favorite';
        console.log('Hello AppDataProvider Provider');
        //this.authorizeURL = this.spotifyApi.createAuthorizeURL(this.scopes, state);
        this.savedCategory = [];
        this.favoriteMusic = [];
        this.recoverStorage();
    }
    // Cut Url Login
    AppDataProvider.prototype.getLogin = function (url) {
        // if the query string is NULL
        var _this = this;
        this.params = new Map();
        var queries = url.split(new RegExp('[&#]', 'g'));
        queries.forEach(function (indexQuery) {
            var indexPair = indexQuery.split("=");
            var queryKey = decodeURIComponent(indexPair[0]);
            var queryValue = decodeURIComponent(indexPair.length > 1 ? indexPair[1] : "");
            _this.params[queryKey] = queryValue;
        });
        this.spotifyApi.setAccessToken(this.params.access_token);
        return this.params;
    };
    // Access Token
    AppDataProvider.prototype.getHomeUrl = function () {
        this.urlAccess = this.params.access_token;
        return this.urlAccess;
    };
    // Info User
    AppDataProvider.prototype.getUserId = function () {
        var _this = this;
        this.spotifyApi.getMe()
            .then(function (data) {
            console.log('Some information about the authenticated user');
            var response = data.body;
            console.log('user.data');
            console.log(response);
            _this.idUserApp = response.id;
        }, function (err) {
            console.log('Something went wrong!', err);
        });
    };
    /* Set Country*/
    AppDataProvider.prototype.setCountryCode = function (country) {
        this.country = country;
        console.log("seteo country code");
        console.log(this.country);
        this.updateStorage();
    };
    AppDataProvider.prototype.deletecountry = function () {
        this.country = null;
        console.log("cambiar pais");
        console.log(this.country);
        this.updateStorage();
    };
    /**/
    AppDataProvider.prototype.addCategory = function (id, name) {
        var isCategorySelected = false;
        for (var i = 0; i < this.savedCategory.length; i++) {
            if (this.savedCategory[i].id === id) {
                // If product exist in cart
                // we update just quantity and subtotal
                this.savedCategory.splice(i, 1);
                isCategorySelected = true;
                console.log("Eliminar array");
                console.log(this.savedCategory);
            }
        }
        if (!isCategorySelected) {
            var newCategory = {};
            newCategory.id = id;
            newCategory.name = name;
            newCategory.isSelected = true;
            this.savedCategory.push(newCategory);
            console.log(this.savedCategory);
        }
        this.updateStorage();
    };
    AppDataProvider.prototype.returnCategories = function () {
        return this.savedCategory;
    };
    AppDataProvider.prototype.deleteCategories = function () {
        this.savedCategory = [];
        this.updateStorage();
    };
    /* Favorites Playlist or Artist */
    AppDataProvider.prototype.addFavorite = function (id, name, url, type, owner) {
        var isFavorite = false;
        for (var i = 0; i < this.favoriteMusic.length; i++) {
            if (this.favoriteMusic[i].id === id) {
                this.favoriteMusic.splice(i, 1);
                isFavorite = true;
                console.log(this.favoriteMusic);
            }
        }
        if (!isFavorite) {
            var newFavorite = {};
            newFavorite.id = id;
            newFavorite.name = name;
            newFavorite.url = url;
            newFavorite.type = type;
            newFavorite.owner = owner;
            this.favoriteMusic.push(newFavorite);
            console.log(this.favoriteMusic);
        }
        this.updateStorage();
    };
    AppDataProvider.prototype.checkIsFavorite = function (id) {
        for (var i = 0; i < this.favoriteMusic.length; i++) {
            if (this.favoriteMusic[i].id === id) {
                return true;
            }
        }
    };
    AppDataProvider.prototype.deleteFavorite = function (id) {
        for (var i = 0; i < this.favoriteMusic.length; i++) {
            if (this.favoriteMusic[i].id === id) {
                this.favoriteMusic.splice(i, 1);
                console.log(this.favoriteMusic);
            }
        }
    };
    AppDataProvider.prototype.returnFavorites = function () {
        return this.favoriteMusic;
    };
    /* local Storage*/
    AppDataProvider.prototype.recoverStorage = function () {
        var _this = this;
        // Recover country code
        this.localStorage.get(this.COUNTRY_SELECTED_KEY).then(function (val) {
            if (val) {
                _this.country = val;
                console.log("HAY PAIS");
                _this.recoverCategorySaved();
                //this._cartDataRecovered = true;
                //this.events.publish('cartLoaded',val);
            }
            else {
                _this.savedCategory = [];
                console.log("NO HAY NADA");
            }
        });
        this.localStorage.get(this.ARTIST_PLAYLIST_FAVORITE).then(function (val) {
            if (val) {
                _this.favoriteMusic = val;
            }
            else {
                _this.favoriteMusic = [];
            }
        });
    };
    AppDataProvider.prototype.recoverCategorySaved = function () {
        var _this = this;
        // Recover Category
        this.localStorage.get(this.CATEGORIES_SELECTED_KEY).then(function (val) {
            if (val) {
                _this.savedCategory = val;
                console.log("CATEGORIAS DESDE LOCAL STORAGE");
                console.log(_this.savedCategory);
                console.log("///////////////////");
                //this._cartProductsRecovered = true;
            }
        });
    };
    // Update stored cart
    AppDataProvider.prototype.updateStorage = function () {
        console.log("GUARDAR LOCAL STORAGE");
        // COUNTRY
        this.localStorage.set(this.COUNTRY_SELECTED_KEY, this.country);
        // CATEGORYS
        this.localStorage.set(this.CATEGORIES_SELECTED_KEY, this.savedCategory);
        // FAVORITE
        this.localStorage.set(this.ARTIST_PLAYLIST_FAVORITE, this.favoriteMusic);
    };
    return AppDataProvider;
}());
AppDataProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */]) === "function" && _b || Object])
], AppDataProvider);

var _a, _b;
//# sourceMappingURL=app-data.js.map

/***/ }),

/***/ 162:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/artist/artist.module": [
		296,
		9
	],
	"../pages/detail-playlist/detail-playlist.module": [
		297,
		8
	],
	"../pages/detail/detail.module": [
		295,
		7
	],
	"../pages/intro/intro.module": [
		292,
		6
	],
	"../pages/like/like.module": [
		299,
		5
	],
	"../pages/login/login.module": [
		300,
		4
	],
	"../pages/playlist/playlist.module": [
		301,
		3
	],
	"../pages/profile/profile.module": [
		293,
		2
	],
	"../pages/relatedartist/relatedartist.module": [
		294,
		1
	],
	"../pages/search/search.module": [
		298,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 162;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 210:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(229);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 229:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(163);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_in_app_browser__ = __webpack_require__(167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(166);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__(291);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_home_home__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_login_login__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_profile_profile__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_search_search__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_artist_artist__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_playlist_playlist__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_relatedartist_relatedartist__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_detail_detail__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_detail_playlist_detail_playlist__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_intro_intro__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_like_like__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ionic_native_status_bar__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__ionic_native_splash_screen__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__providers_app_data_app_data__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





















var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_8__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_profile_profile__["a" /* ProfilePage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_search_search__["a" /* SearchPage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_artist_artist__["a" /* ArtistPage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_playlist_playlist__["a" /* PlaylistPage */],
            __WEBPACK_IMPORTED_MODULE_13__pages_relatedartist_relatedartist__["a" /* RelatedartistPage */],
            __WEBPACK_IMPORTED_MODULE_14__pages_detail_detail__["a" /* DetailPage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_detail_playlist_detail_playlist__["a" /* DetailPlaylistPage */],
            __WEBPACK_IMPORTED_MODULE_16__pages_intro_intro__["a" /* IntroPage */],
            __WEBPACK_IMPORTED_MODULE_17__pages_like_like__["a" /* LikePage */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */], {}, {
                links: [
                    { loadChildren: '../pages/intro/intro.module#IntroPageModule', name: 'IntroPage', segment: 'intro', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/profile/profile.module#ProfilePageModule', name: 'ProfilePage', segment: 'profile', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/relatedartist/relatedartist.module#RelatedartistPageModule', name: 'RelatedartistPage', segment: 'relatedartist', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/detail/detail.module#DetailPageModule', name: 'DetailPage', segment: 'detail', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/artist/artist.module#ArtistPageModule', name: 'ArtistPage', segment: 'artist', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/detail-playlist/detail-playlist.module#DetailPlaylistPageModule', name: 'DetailPlaylistPage', segment: 'detail-playlist', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/search/search.module#SearchPageModule', name: 'SearchPage', segment: 'search', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/like/like.module#LikePageModule', name: 'LikePage', segment: 'like', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/playlist/playlist.module#PlaylistPageModule', name: 'PlaylistPage', segment: 'playlist', priority: 'low', defaultHistory: [] }
                ]
            }),
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_8__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_profile_profile__["a" /* ProfilePage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_search_search__["a" /* SearchPage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_artist_artist__["a" /* ArtistPage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_playlist_playlist__["a" /* PlaylistPage */],
            __WEBPACK_IMPORTED_MODULE_13__pages_relatedartist_relatedartist__["a" /* RelatedartistPage */],
            __WEBPACK_IMPORTED_MODULE_14__pages_detail_detail__["a" /* DetailPage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_detail_playlist_detail_playlist__["a" /* DetailPlaylistPage */],
            __WEBPACK_IMPORTED_MODULE_16__pages_intro_intro__["a" /* IntroPage */],
            __WEBPACK_IMPORTED_MODULE_17__pages_like_like__["a" /* LikePage */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_18__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_19__ionic_native_splash_screen__["a" /* SplashScreen */],
            { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicErrorHandler */] },
            __WEBPACK_IMPORTED_MODULE_20__providers_app_data_app_data__["a" /* AppDataProvider */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_in_app_browser__["a" /* InAppBrowser */],
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 291:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_login_login__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_profile_profile__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_playlist_playlist__ = __webpack_require__(112);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* LoginPage */];
        this.initializeApp();
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Login', component: __WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* LoginPage */] },
            { title: 'Inicio', component: __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */] },
            { title: 'Descubre Playlists', component: __WEBPACK_IMPORTED_MODULE_7__pages_playlist_playlist__["a" /* PlaylistPage */] },
            { title: 'Tu Perfil', component: __WEBPACK_IMPORTED_MODULE_6__pages_profile_profile__["a" /* ProfilePage */] },
        ];
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    return MyApp;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_14" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Nav */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Nav */])
], MyApp.prototype, "nav", void 0);
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"D:\htdocs\Ionic\sincronizapc\src\app\app.html"*/'<ion-menu [content]="content">\n  <ion-header>\n    <ion-toolbar>\n      <ion-title>Menu</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content>\n    <ion-list>\n      <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n        {{p.title}}\n      </button>\n    </ion-list>\n  </ion-content>\n\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"D:\htdocs\Ionic\sincronizapc\src\app\app.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 41:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IntroPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_app_data_app_data__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the IntroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var IntroPage = (function () {
    function IntroPage(navCtrl, navParams, appData, render) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.appData = appData;
        this.render = render;
        this.isCategoriesUpdated = false;
        this.isCountrySelected = false;
        this.country = this.appData.country;
        this.listCountry = this.appData.countries;
        this.categoriesSelected = this.appData.returnCategories();
        this.accessSpotify = this.appData.spotifyApi;
        this.isCountrySelected = false;
        console.log(this.listCountry);
    }
    IntroPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad IntroPage');
        //console.log(this.appData.country.code);
        //this.seeSelectedCategories();
        if (this.country == null) {
            console.log("pais no escogiddo");
            this.isCountrySelected = false;
        }
        else {
            console.log("pais ya escogido");
            this.isCountrySelected = true;
            this.countrySelected(this.appData.country);
        }
    };
    IntroPage.prototype.countrySelected = function (country) {
        this.appData.setCountryCode(country);
        this.isCountrySelected = true;
        this.getCountryCategories(country.code);
    };
    IntroPage.prototype.getCountryCategories = function (code) {
        var _this = this;
        this.accessSpotify.getCategories({
            limit: 6,
            offset: 0,
            country: code,
        })
            .then(function (data) {
            console.log('Artist Data');
            var response = data.body;
            _this.categories = response.categories.items;
            //console.log(this.categories);
            _this.isCategoriesUpdated = true;
        }, function (err) {
            console.log('Something went wrong!', err);
        });
    };
    IntroPage.prototype.checkCategory = function (id, name) {
        //this.isSelected = !this.isSelected;
        this.render.setElementClass(event.target, "selected", this.isSelected);
        this.appData.addCategory(id, name);
    };
    /**/
    IntroPage.prototype.checkIsSelected = function (id) {
        for (var i = 0; i < this.categoriesSelected.length; i++) {
            if (id === this.categoriesSelected[i].id) {
                var id_read = this.categoriesSelected[i].id;
                return id_read;
            }
        }
    };
    IntroPage.prototype.goHome = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
    };
    return IntroPage;
}());
IntroPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-intro',template:/*ion-inline-start:"D:\htdocs\Ionic\sincronizapc\src\pages\intro\intro.html"*/'<!--\n  Generated template for the IntroPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-content padding  class="back-login">\n	<img src="./assets/imgs/logo.png" alt="" id="logo-c">\n	<h1>Sincroniza</h1>\n	<div col-12 *ngIf="!isCountrySelected">\n		<p>\n		Selecciona tu pais de origen\n		</p>	\n		<div col-6 class="country-c" *ngFor="let coun of listCountry" >\n			<button class="int-country" (click)="countrySelected(coun)">\n				<img src="./assets/imgs/{{coun.flag}}" alt=""> <span>{{coun.name}}</span>\n			</button>\n		</div>\n	</div>\n	<div col-12 *ngIf="isCategoriesUpdated">\n		<p> \n		Selecciona preferencia musical\n		</p>\n		<div *ngFor="let category of categories" class="cont-categories">\n			<span (click)="checkCategory(category.id,category.name)" [ngClass]="category.id === checkIsSelected(category.id) ? \'selected\' : \'no-selected\'">{{category.name}}</span>\n			<!--=<span (click)="selectCategory($event,category.id,category.name)">{{category.name}}</span>-->\n		</div>\n		<button ion-button (click)="goHome();" class="btn-green">Empezar</button>\n	</div>\n</ion-content>\n'/*ion-inline-end:"D:\htdocs\Ionic\sincronizapc\src\pages\intro\intro.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_app_data_app_data__["a" /* AppDataProvider */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Renderer */]])
], IntroPage);

//# sourceMappingURL=intro.js.map

/***/ }),

/***/ 42:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ArtistPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__relatedartist_relatedartist__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__detail_detail__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_app_data_app_data__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the ArtistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ArtistPage = (function () {
    function ArtistPage(navCtrl, navParams, appData, toast) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.appData = appData;
        this.toast = toast;
        this.dataUp = false;
        this.discUp = false;
        this.isFavorite = false;
        this.accessSpotify = this.appData.spotifyApi;
        this.idSearch = this.navParams.get('paramId');
        this.isFavorite = this.appData.checkIsFavorite(this.idSearch);
    }
    ArtistPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad ArtistPage');
        // Get an artist
        this.accessSpotify.getArtist(this.idSearch)
            .then(function (data) {
            console.log('Artist Data');
            var response = data.body;
            _this.artistInfo = response;
            _this.dataUp = true;
            console.log(_this.artistInfo);
        }, function (err) {
            console.log('Something went wrong!', err);
        });
        // Get Discografy
        this.accessSpotify.getArtistAlbums(this.idSearch)
            .then(function (data) {
            console.log('Artist Data');
            var response = data.body;
            _this.artistAlbums = response.items;
            _this.discUp = true;
            //console.log(this.artistAlbums);
        }, function (err) {
            console.log('Something went wrong!', err);
        });
    };
    ArtistPage.prototype.goRelated = function (id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__relatedartist_relatedartist__["a" /* RelatedartistPage */], { idArtist: id });
    };
    /*Detail Album*/
    ArtistPage.prototype.goDetailAlbum = function (id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__detail_detail__["a" /* DetailPage */], { idAlbum: id });
    };
    /*Add Favorite Artist*/
    ArtistPage.prototype.addFavoriteArtist = function (id, name, img, type) {
        this.isFavorite = !this.isFavorite;
        var owner = "null";
        this.appData.addFavorite(id, name, img, type, owner);
        var toast = this.toast.create({
            message: 'Artista Guardado',
            duration: 3000,
            position: 'bottom'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    return ArtistPage;
}());
ArtistPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-artist',template:/*ion-inline-start:"D:\htdocs\Ionic\sincronizapc\src\pages\artist\artist.html"*/'<!--\n  Generated template for the ArtistPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-icon id="app-icon"><img src="./assets/imgs/logo.png" alt=""></ion-icon>\n    <button class="icon-home"><img src="./assets/icon/icon_notification.png" alt=""></button>\n    <button class="icon-home" (click)="getHomeSearch()"><img src="./assets/icon/icon_search.png" alt=""></button>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n  <div col-12 *ngIf="dataUp">\n  	<h1>{{artistInfo.name}}</h1>\n  	<div id="cont-artist">      \n  		<img src="{{artistInfo.images[0].url}}" alt="">\n  		<div id="artist-related" (click)="goRelated(artistInfo.id)">\n  			<img src="./assets/icon/artist_related.png" alt="">\n  		</div>\n  		<button id="button_like" [class.active]="isFavorite" (click)="addFavoriteArtist(artistInfo.id,artistInfo.name,artistInfo.images[1].url,artistInfo.type)"><ion-icon name="heart" class="icon"></ion-icon></button>\n  	</div>\n    <div id="cont-discografia" *ngIf="discUp">\n      <h1><img src="./assets/icon/icon_disc.png" alt=""><span>Discografía</span></h1>\n      <div class="container-fluid cont-disco" *ngFor="let album of artistAlbums">\n        <div col-5 class="img-disc">\n          <img src="{{album.images[0].url}}" alt="">\n        </div>\n        <div col-7 class="detail-disc">\n          <span class="name-disc">{{album.name}}</span>\n          <!--<span class="year-disc">2010</span>-->\n          <button (click)="goDetailAlbum(album.id)"><img src="./assets/icon/icon_songs.png" alt=""></button>\n        </div>\n      </div>\n    </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"D:\htdocs\Ionic\sincronizapc\src\pages\artist\artist.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__providers_app_data_app_data__["a" /* AppDataProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ToastController */]])
], ArtistPage);

//# sourceMappingURL=artist.js.map

/***/ }),

/***/ 43:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailPlaylistPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_app_data_app_data__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the DetailPlaylistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DetailPlaylistPage = (function () {
    function DetailPlaylistPage(navCtrl, navParams, appData, toast) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.appData = appData;
        this.toast = toast;
        this.detailUp = false;
        this.isFavorite = false;
        this.accessSpotify = this.appData.spotifyApi;
        this.ownerId = this.navParams.get('ownerId');
        this.idPlaylist = this.navParams.get('playlistId');
        this.isFavorite = this.appData.checkIsFavorite(this.idPlaylist);
    }
    DetailPlaylistPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad DetailPage');
        this.accessSpotify.getPlaylist(this.ownerId, this.idPlaylist)
            .then(function (data) {
            console.log('Artist Data');
            var response = data.body;
            console.log(response);
            console.log('Body del playlist');
            _this.playlistDetail = response;
            _this.playlistTracks = response.tracks.items;
            _this.detailUp = true;
            //console.log(this.playlistTracks);
        }, function (err) {
            console.log('Something went wrong!', err);
        });
    };
    /**/
    DetailPlaylistPage.prototype.addFavoritePlaylist = function (id, name, img, type, owner) {
        this.isFavorite = !this.isFavorite;
        this.appData.addFavorite(id, name, img, type, owner);
        var toast = this.toast.create({
            message: 'Playlist Guardado',
            duration: 3000,
            position: 'bottom'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    return DetailPlaylistPage;
}());
DetailPlaylistPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-detail-playlist',template:/*ion-inline-start:"D:\htdocs\Ionic\sincronizapc\src\pages\detail-playlist\detail-playlist.html"*/'<!--\n  Generated template for the DetailPlaylistPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n    <ion-navbar>\n    <ion-icon id="app-icon"><img src="./assets/imgs/logo.png" alt=""></ion-icon>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n	<div col-12 class="back2" id="albumDetail" *ngIf="detailUp">\n		<div class="cont-album">\n			<img src="{{playlistDetail.images[0].url}}">\n			<div class="infoAlbum">\n				<h3>{{playlistDetail.name}}</h3>\n				<span>{{playlistDetail.release_date}}</span>\n				<span>{{playlistDetail.tracks.total}} canciones</span>\n			</div>\n		</div>\n		<button id="button_like" [class.active]="isFavorite" (click)="addFavoritePlaylist(playlistDetail.id,playlistDetail.name,playlistDetail.images[0].url,playlistDetail.type,playlistDetail.owner.id)"><ion-icon name="heart" class="icon"></ion-icon></button>\n	</div>\n	<div class="cont-list">\n		<h2>Listado de Canciones</h2>\n		<ion-list inset>\n		  <ion-item *ngFor="let track of playlistTracks">\n		     {{track.track.name}}\n		  </ion-item> \n		</ion-list>\n	</div>\n</ion-content>\n'/*ion-inline-end:"D:\htdocs\Ionic\sincronizapc\src\pages\detail-playlist\detail-playlist.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_app_data_app_data__["a" /* AppDataProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ToastController */]])
], DetailPlaylistPage);

//# sourceMappingURL=detail-playlist.js.map

/***/ }),

/***/ 50:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_app_data_app_data__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__profile_profile__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__search_search__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__detail_detail__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__detail_playlist_detail_playlist__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__like_like__ = __webpack_require__(110);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





//import { ArtistPage } from '../artist/artist';
//import { PlaylistPage } from '../playlist/playlist';



var HomePage = (function () {
    function HomePage(navCtrl, appData) {
        this.navCtrl = navCtrl;
        this.appData = appData;
        this.urlPrueba = this.appData.getHomeUrl();
        this.accessSpotify = this.appData.spotifyApi;
        this.countryHome = this.appData.country.code;
        this.categoriesHome = this.appData.returnCategories();
        this.categoriesComplete = [];
        console.log('CATEGORIES');
        console.log(this.categoriesHome);
        this.appData.getUserId();
    }
    HomePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad HomePage');
        /* NUEVOS LANZAMIENTOS */
        this.accessSpotify.getNewReleases({ limit: 5, offset: 0, country: this.countryHome })
            .then(function (data) {
            var response = data.body;
            //console.log(response);
            _this.newRelease = response.albums.items;
        }, function (err) {
            console.log('Something went wrong!', err);
        });
        /*  */
        for (var i in this.categoriesHome) {
            // 1, "string", false
            console.log('GoFOR');
            console.log(this.categoriesHome[i].id);
            this.accessSpotify.getPlaylistsForCategory(this.categoriesHome[i].id, {
                country: this.countryHome,
                limit: 2,
                offset: 0
            })
                .then(function (data) {
                var response = data.body;
                //console.log(response);
                //this.categoriesComplete = response;          
                for (var x in response.playlists.items) {
                    console.log(x);
                    _this.categoriesComplete.push(response.playlists.items[x]);
                    console.log(_this.categoriesComplete);
                }
            }, function (err) {
                console.log('Something went wrong!', err);
            });
        }
    };
    HomePage.prototype.goRelease = function (id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__detail_detail__["a" /* DetailPage */], { idAlbum: id });
    };
    HomePage.prototype.playlistDetail = function (id, owner) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__detail_playlist_detail_playlist__["a" /* DetailPlaylistPage */], { playlistId: id, ownerId: owner });
    };
    /**/
    HomePage.prototype.getMe = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__profile_profile__["a" /* ProfilePage */]);
    };
    ;
    HomePage.prototype.getHomeSearch = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__search_search__["a" /* SearchPage */]);
    };
    HomePage.prototype.getLikesPage = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__like_like__["a" /* LikePage */]);
    };
    return HomePage;
}());
HomePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-home',template:/*ion-inline-start:"D:\htdocs\Ionic\sincronizapc\src\pages\home\home.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-icon id="app-icon"><img src="./assets/imgs/logo.png" alt=""></ion-icon>\n    <button class="icon-home" (click)="getLikesPage()"><img src="./assets/icon/icon_notification.png" alt=""></button>\n    <button class="icon-home" (click)="getHomeSearch()"><img src="./assets/icon/icon_search.png" alt=""></button>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n   <!-- slides home -->\n   <ion-slides id="slide-home">\n      <ion-slide *ngFor="let release of newRelease">\n        <img src="{{release.images[0].url}}" alt="">\n        <div class="info-slide">\n          <h1>{{release.name}}</h1>\n          <button (click)="goRelease(release.id);">Descubrir</button>\n        </div>\n      </ion-slide>\n  </ion-slides>\n  <!-- artist \n  <ion-grid>\n    <ion-row>\n      <ion-col col-6>\n        <div class="cont-artist">\n          <div class="img-thumb"><img src="./assets/imgs/thumb.png" alt=""></div>\n          <h2>Nombre del artista</h2>\n          <span>descubrir</span>\n        </div>\n      </ion-col>\n      <ion-col col-6 class="back2">\n        <div class="cont-artist">\n          <div class="img-thumb"><img src="./assets/imgs/thumb.png" alt=""></div>\n          <h2>Nombre del artista</h2>\n          <span>descubrir</span>\n        </div>\n      </ion-col>\n      <ion-col col-6 class="back2">\n        <div class="cont-artist">\n          <div class="img-thumb"><img src="./assets/imgs/thumb.png" alt=""></div>\n          <h2>Nombre del artista</h2>\n          <span>descubrir</span>\n        </div>\n      </ion-col>\n      <ion-col col-6>\n        <div class="cont-artist">\n          <div class="img-thumb"><img src="./assets/imgs/thumb.png" alt=""></div>\n          <h2>Nombre del artista</h2>\n          <span>descubrir</span>\n        </div>\n      </ion-col>      \n    </ion-row>\n  </ion-grid>\n  <!--\n  <button id="btn-action-green">Descubre más artistas</button>\n  <!---->\n  <section id="playlist">\n    <div class="title-select">\n      <div class="title"><span>Recomendaciones</span><br/>para ti</div>\n    </div>\n  </section>\n  <ion-grid>\n    <ion-row>\n      <ion-col col-6 class="cont-playlist" *ngFor="let playlist of categoriesComplete, let index">        \n        <img src="{{playlist.images[0].url}}" alt="">\n         <span (click)=\'playlistDetail(playlist.id, playlist.owner.id)\' class="btn-playlist">ver playlist</span>\n      </ion-col>                 \n    </ion-row>\n  </ion-grid>\n  <!-- \n  <button id="btn-action-green">Descubre playlist</button>\n  -->\n</ion-content>\n'/*ion-inline-end:"D:\htdocs\Ionic\sincronizapc\src\pages\home\home.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_app_data_app_data__["a" /* AppDataProvider */]])
], HomePage);

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 55:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_app_data_app_data__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__intro_intro__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ProfilePage = (function () {
    function ProfilePage(navCtrl, navParams, appData) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.appData = appData;
        this.accessSpotify = this.appData.spotifyApi;
        this.userCountry = this.appData.country;
        this.userCategories = this.appData.savedCategory;
    }
    ProfilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProfilePage');
        //this.infoProfile = this.appData.getMeInfo();
        this.isProfile = false;
        console.log(this.userCategories);
        this.getProfile();
    };
    ProfilePage.prototype.getProfile = function () {
        var _this = this;
        this.accessSpotify.getMe()
            .then(function (data) {
            console.log('Some information about the authenticated user');
            console.log(data.body);
            var response = data.body;
            _this.userProfile = response;
            _this.isProfile = true;
        }, function (err) {
            console.log('Something went wrong!', err);
        });
    };
    /**/
    ProfilePage.prototype.changeCountry = function () {
        this.appData.deletecountry();
        this.appData.deleteCategories();
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__intro_intro__["a" /* IntroPage */]);
    };
    /**/
    ProfilePage.prototype.manageCategories = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__intro_intro__["a" /* IntroPage */]);
    };
    return ProfilePage;
}());
ProfilePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-profile',template:/*ion-inline-start:"D:\htdocs\Ionic\sincronizapc\src\pages\profile\profile.html"*/'<!--\n  Generated template for the ProfilePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-icon id="app-icon"><img src="./assets/imgs/logo.png" alt=""></ion-icon>\n    <button class="icon-home" (click)="getLikesPage()"><img src="./assets/icon/icon_notification.png" alt=""></button>\n    <button class="icon-home" (click)="getHomeSearch()"><img src="./assets/icon/icon_search.png" alt=""></button>\n  </ion-navbar>\n</ion-header>\n\n\n\n<ion-content>\n\n	<div id="user_profile" *ngIf="isProfile">\n		<img src="{{userProfile.images[0].url}}">\n		<span>{{userProfile.display_name}}</span>\n	</div>\n	\n    <div class="title-select">\n	   <div class="title"><span>País Seleccionado</span></div>\n	</div>\n	<div col-12 class="info-country">\n		<div class="country-p">\n			<div class="int-country">\n				<img src="./assets/imgs/{{userCountry.flag}}" alt=""><span>{{userCountry.name}}</span>\n			</div>\n		</div>\n		<button class="action-btn" (click)="changeCountry()">Cambiar</button>\n	</div>\n	<div class="title-select">\n	   <div class="title"><span>Categorías seleccionadas</span></div>\n	</div>\n	<div col-12>\n		<div class="cont-categories" *ngFor="let cat of userCategories">\n			<span>{{cat.name}}</span>\n			<!--<span (click)="selectCategory($event,category.id,category.name)">{{category.name}}</span>-->\n		</div>\n		<button class="btn-categories" (click)="manageCategories()">Añadir / Borrar</button>\n	</div>\n</ion-content>\n'/*ion-inline-end:"D:\htdocs\Ionic\sincronizapc\src\pages\profile\profile.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_app_data_app_data__["a" /* AppDataProvider */]])
], ProfilePage);

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 56:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_app_data_app_data__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Subject__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Subject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__artist_artist__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__detail_playlist_detail_playlist__ = __webpack_require__(43);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SearchPage = (function () {
    function SearchPage(navCtrl, navParams, appData) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.appData = appData;
        this.type = "artist";
        this.activeArtist = true;
        this.activePlaylist = false;
        this.wordTyped$ = new __WEBPACK_IMPORTED_MODULE_3_rxjs_Subject__["Subject"]();
        this.isSearched = false;
        this.accessTokenSearch = this.appData.spotifyApi;
        this.wordTyped$.subscribe(function (word) { return _this.searchArtist(word); });
    }
    SearchPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SearchPage');
    };
    /**/
    SearchPage.prototype.searchArtista = function () {
        this.type = "artist";
        this.activePlaylist = false;
        this.activeArtist = true;
        //this.searchArtist();
    };
    SearchPage.prototype.searchPlaylist = function () {
        this.type = "playlist";
        this.activeArtist = false;
        this.activePlaylist = true;
        //this.searchArtist();
    };
    /**/
    SearchPage.prototype.searchArtist = function (word) {
        var _this = this;
        console.log(word);
        if (this.type == 'artist') {
            this.accessTokenSearch.searchArtists(word, { limit: 10 })
                .then(function (data) {
                var artist = data.body;
                _this.resultSearch = artist.artists.items;
                _this.isSearched = true;
                //console.log(this.resultSearch);
            }, function (err) {
                console.log('Something went wrong!', err);
            });
        }
        else if (this.type == "playlist") {
            this.accessTokenSearch.searchPlaylists(word, { limit: 10 })
                .then(function (data) {
                var playlist = data.body;
                _this.resultSearch = playlist.playlists.items;
                _this.isSearched = true;
                //console.log(this.resultSearch);
            }, function (err) {
                console.log('Something went wrong!', err);
            });
        }
    };
    SearchPage.prototype.searchChoose = function (id, type, owner) {
        console.log(type);
        if (type == "artist") {
            console.log("choose artist");
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__artist_artist__["a" /* ArtistPage */], { paramId: id });
        }
        else if (type == "playlist") {
            console.log("choose playlist");
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__detail_playlist_detail_playlist__["a" /* DetailPlaylistPage */], { playlistId: id, ownerId: owner });
        }
    };
    return SearchPage;
}());
SearchPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-search',template:/*ion-inline-start:"D:\htdocs\Ionic\sincronizapc\src\pages\search\search.html"*/'<!--\n  Generated template for the SearchPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-icon id="app-icon"><img src="./assets/imgs/logo.png" alt=""></ion-icon>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n	<!--<img src="./assets/icon/icon_search.svg" alt="" class="icon_big">-->\n	<div col-12>\n		<div col-6 class="btn-type" [class.active]="activeArtist" (click)="searchArtista()">Artista</div>\n		<div col-6 class="btn-type" [class.active]="activePlaylist" (click)="searchPlaylist()">Playlist</div>\n	</div>\n	<div class="cont-input">\n		<label>Buscar artista</label>\n		<input (input)="wordTyped$.next($event.target.value)" placeholder="Escribe..">\n	</div>\n	<div class="cont-seach" *ngIf=(isSearched);>\n		<h2>Resultado de Búsqueda</h2>\n		<ion-list inset>\n		  <div *ngFor="let result of resultSearch">\n		    <button ion-item class="back2" (click)="searchChoose(result.id,result.type,result.owner?.id)">{{result.name}}</button>\n		  </div> \n		</ion-list>\n	</div>\n</ion-content>\n'/*ion-inline-end:"D:\htdocs\Ionic\sincronizapc\src\pages\search\search.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_app_data_app_data__["a" /* AppDataProvider */]])
], SearchPage);

//# sourceMappingURL=search.js.map

/***/ }),

/***/ 57:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_app_data_app_data__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the DetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DetailPage = (function () {
    function DetailPage(navCtrl, navParams, appData) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.appData = appData;
        this.detailUp = false;
        this.accessSpotify = this.appData.spotifyApi;
        this.idAlbum = this.navParams.get('idAlbum');
    }
    DetailPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad DetailPage');
        this.accessSpotify.getAlbum(this.idAlbum)
            .then(function (data) {
            console.log('Artist Data');
            var response = data.body;
            _this.albumDetail = response;
            _this.albumTracks = response.tracks.items;
            _this.detailUp = true;
            console.log(_this.albumTracks);
        }, function (err) {
            console.log('Something went wrong!', err);
        });
    };
    return DetailPage;
}());
DetailPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-detail',template:/*ion-inline-start:"D:\htdocs\Ionic\sincronizapc\src\pages\detail\detail.html"*/'<!--\n  Generated template for the DetailPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n    <ion-navbar>\n    <ion-icon id="app-icon"><img src="./assets/imgs/logo.png" alt=""></ion-icon>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n	<div col-12 class="back2" id="albumDetail" *ngIf="detailUp">\n		<div class="cont-album">\n			<img src="{{albumDetail.images[0].url}}">\n			<div class="infoAlbum">\n				<h3>{{albumDetail.name}}</h3>\n				<span>{{albumDetail.release_date}}</span>\n				<span>{{albumDetail.tracks.total}} canciones</span>\n			</div>\n		</div>\n	</div>\n	<div class="cont-list">\n		<h2>Listado de Canciones</h2>\n		<ion-list inset>\n		  <ion-item *ngFor="let track of albumTracks">\n		   <span>{{track.track_number}}</span>   {{track.name}}\n		  </ion-item> \n		</ion-list>\n	</div>\n</ion-content>\n'/*ion-inline-end:"D:\htdocs\Ionic\sincronizapc\src\pages\detail\detail.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_app_data_app_data__["a" /* AppDataProvider */]])
], DetailPage);

//# sourceMappingURL=detail.js.map

/***/ })

},[210]);
//# sourceMappingURL=main.js.map